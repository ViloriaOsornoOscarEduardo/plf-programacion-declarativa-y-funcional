# Programación Declarativa Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
* Programación Declarativa.\n Orientaciones  \n y pautas para el estudio \n(1998, 1999, 2001)
** Qué es?
*** en contraposición a \nla programación imperativa
*** paradigma
** a que se encarga?
*** facilita la programacion
*** especifica el resultado deseado
** basado en?
*** logica
*** matematica
** un conjunto
*** condiciones
*** proposicione
*** afirmaciones
*** restricciones
*** ecuaciones 
*** transformaciones
** Tipos
*** Los lenguajes lógicos
****_ como 
***** Prolog.
*** Los lenguajes algebraicos
****_ como
***** Maude 
***** SQL.
*** Los lenguajes funcionales
****_ como
***** Haskell 
***** Erlang.
** lenguajes declarativos
***_ como
**** Haskell 
**** ML 
**** Lisp
**** Prolog
**** F-Prolog
**** Curry
**** SQL
**** QML
** Ventajas
*** fiables
*** expresivos
** encargado
*** compilador
@endmindmap
```

# Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
* Lenguaje de Programación Funcional (2015)
** que es?
***  paradigma de programación
*** resolver diferentes problemáticas
** conceptos tales
***_ como
**** Funciones puras.
*****_ se define
****** son más que funciones
**** Composición de funciones.
*****_ se define
****** combinar dos o más funciones
**** Estados compartidos.
*****_ se define
****** alcance global o ámbitos de cierre
**** Mutabilidad.
*****_ se define
******  no puede ser modificado una vez haya sido creado
**** Efecto secundario.
*****_ se define
****** cualquier cambio de estado en la\n aplicación que sea observable\n fuera de la función llamada.
** lenguajes
***_ tales como
**** Java
**** PHP
**** Ruby
**** Python
**** Elixir
**** Kotling
**** Haskell
**** Erlang
** ventajas 
*** software mucho más legible
*** fácil de testear
*** código más corto
*** sencillo
*** legible 
** caracteristicas 
*** No existen efectos colaterales
*** Tiene una semántica limpia
*** combinable con la programación orientada a objetos
@endmindmap
```
